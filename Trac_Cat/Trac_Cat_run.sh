usage="\nTracfone mWeb Daily Behavior Report v1.0\n\nCreated and Maintained by Luigi Weiss\n\nFor assistance, please go to https://3cinteractive.atlassian.net/wiki/display/DT/Jobs%3A+Tracfone+mWeb+Daily+Behavior+Report\n\n"

while getopts :h option
do
        case "${option}"
        in
                h) echo -e "$usage"
                                exit 0
                                ;;
        esac
done

cd `dirname $0`
 ROOT_PATH=`pwd`
 java -Xms256M -Xmx1024M -cp $ROOT_PATH/../lib/camel-core-2.10.4.jar:$ROOT_PATH/../lib/checkArchive.jar:$ROOT_PATH/../lib/commons-compress-1.4.1.jar:$ROOT_PATH/../lib/dom4j-1.6.1.jar:$ROOT_PATH/../lib/external_sort.jar:$ROOT_PATH/../lib/jakarta-oro-2.0.8.jar:$ROOT_PATH/../lib/jxl.jar:$ROOT_PATH/../lib/talend_file_enhanced_20070724.jar:$ROOT_PATH/../lib/talendcsv.jar:$ROOT_PATH:$ROOT_PATH/../lib/systemRoutines.jar::$ROOT_PATH/../lib/userRoutines.jar::.:$ROOT_PATH/trac_cat_1_0.jar: trac_cat.trac_cat_1_0.Trac_Cat --context=Production "$@" 
